package exercices;


import java.util.Map;
import java.util.TreeMap;

public class Question01 {

  public static void main(String[] args) {
      
      Map<String,Integer> repartVille= new TreeMap(); 
     
      System.out.println("\nNombre de personnes par villes\n");
   
      repartVille.put("Arras", 5);
      repartVille.put("Bapaume", 1);      
      repartVille.put("Lens", 5);
      repartVille.put("Vitry-En-Artois", 1);
      
        for ( Map.Entry ent : repartVille.entrySet()) {
        
            System.out.println(ent.getKey()+ " "+ ent.getValue());   
        }      
  
  }
}



